//
// Created by markp on 16.09.2020.
//

#include "include/ICompact.h"

static void tryLog(ILogger * logger, const char * message, ReturnCode code) {
    if (!logger) {
        return;
    }
    logger->log(message, code);
}

static void logError(ILogger * logger, ReturnCode code) {
    switch (code) {
        case ReturnCode::RC_WRONG_DIM:
            tryLog(logger, "compact: operands have different _dim", ReturnCode::RC_WRONG_DIM);
            break;
        case ReturnCode::RC_NO_MEM:
            tryLog(logger, "compact: cannot allocate memory", ReturnCode::RC_WRONG_DIM);
            break;
        case ReturnCode::RC_NULL_PTR:
            tryLog(logger, "compact: cannot operate nullptr", ReturnCode::RC_NULL_PTR);
            break;
        case ReturnCode::RC_INVALID_PARAMS:
            tryLog(logger, "compact: invalid params", ReturnCode::RC_NULL_PTR);
            break;
        case ReturnCode::RC_UNKNOWN:
            tryLog(logger, "compact: unknown error", ReturnCode::RC_UNKNOWN);
            break;
    }
}

namespace {
    enum Order {STRAIGHT = 1, REVERSE = -1};

    class CompactImpl : ICompact {
    public:
        size_t _dim;
        IVector const * _begin, * _end;
        ILogger * _logger;

        ICompact::Iterator * createIterator(const IVector *step, Order order) const;

        class IteratorImpl : ICompact::Iterator {
            IVector const * _begin, * _end, * _step;
            IVector * _cursor;
            std::vector<size_t> _directions;
            Order order;
            ILogger * _logger;
        public:
            IVector* getPoint() const;
            // change order of step
            ReturnCode setDirection(std::vector<size_t> const& direction);
            // adds step to current value in Iterator
            ReturnCode doStep() override;

            IteratorImpl(IVector const * begin, IVector const * end, IVector const * step, std::vector<size_t> directions, Order order);
            ~IteratorImpl();
        };

        ICompact::Iterator * begin(IVector const* step);
        ICompact::Iterator * end(IVector const* step);

        ICompact* clone() const;
        IVector* getBegin() const;
        IVector* getEnd() const;
        ReturnCode contains(IVector const* vec, bool& result) const;
        ReturnCode isSubset(ICompact const* comp, bool& result) const;
        ReturnCode intersects(ICompact const* comp, bool& result) const;
        size_t getDim() const;

        CompactImpl(const IVector * begin, const IVector * end);
        ~CompactImpl() override;
    };

}

ICompact::Iterator *CompactImpl::createIterator(const IVector *step, Order order) const {
    if (!step) {
        logError(_logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    auto begin = _begin->clone();
    if (!begin) {
        logError(_logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    auto end = _end->clone();
    if (!end) {
        logError(_logger, ReturnCode::RC_NULL_PTR);
        delete begin;
        return nullptr;
    }

    std::vector<size_t> directions(_dim);
    for (int i = 0; i < _dim; ++i) {
        directions[i] = i;
    }
    IteratorImpl * result = new(std::nothrow) IteratorImpl(begin, end, step->clone(), directions, order);
    if (!result) {
        logError(_logger, ReturnCode::RC_NULL_PTR);
    }
    return (Iterator *)result;
}

CompactImpl::IteratorImpl::IteratorImpl(IVector const * begin, IVector const * end, IVector const * step, std::vector<size_t> directions, Order order) : _begin(begin), _end(end), _step(step), _directions(directions), order(order) {
    _cursor = order == STRAIGHT ? begin->clone() : end->clone();
    _logger = ILogger::createLogger(this);
}

CompactImpl::IteratorImpl::~IteratorImpl()  {
    delete _begin;
    delete _end;
    delete _step;
    delete _cursor;
    if (_logger) {
        _logger->releaseLogger(this);
        _logger = nullptr;
    }
}

CompactImpl::~CompactImpl() {
    delete _begin;
    delete _end;
    if (_logger) {
        _logger->releaseLogger(this);
        _logger = nullptr;
    }
}

IVector * CompactImpl::getBegin() const {
    return _begin->clone();
}

IVector * CompactImpl::getEnd() const {
    return _end->clone();
}

size_t CompactImpl::getDim() const {
    return _dim;
}

ICompact::Iterator *CompactImpl::begin(const IVector *step) {
    return createIterator(step, STRAIGHT);
}

ICompact::Iterator * CompactImpl::end(const IVector *step) {
    return createIterator(step, REVERSE);
}

IVector * CompactImpl::IteratorImpl::getPoint() const {
    return _cursor->clone();
}

ReturnCode CompactImpl::IteratorImpl::setDirection(const std::vector<size_t> &direction) {
    size_t dim = _begin->getDim();
    if (direction.size() != dim) {
        logError(_logger, ReturnCode::RC_WRONG_DIM);
        return ReturnCode::RC_WRONG_DIM;
    }
    bool all_directions = true;
    std::vector<bool> is_in_direction(dim, false);
    for (int i = 0; i < dim; ++i) {
        is_in_direction[direction[i]] = true;
    }
    for (int i = 0; i < dim; ++i) {
        all_directions = all_directions && is_in_direction[i];
    }
    if (!all_directions) {
        logError(_logger, ReturnCode::RC_INVALID_PARAMS);
        return ReturnCode::RC_INVALID_PARAMS;
    }
    _directions = direction;
    return ReturnCode::RC_SUCCESS;
}

ReturnCode CompactImpl::IteratorImpl::doStep() {
    //find current direction
    size_t  i = 0;
    size_t dim = _begin->getDim();
    for (; i < dim; ++i) {
        double new_value = _cursor->getCoord(i) + _step->getCoord(i) * order;

        if (order == STRAIGHT && new_value > _end->getCoord(i)) {
            _cursor->setCoord(i, _begin->getCoord(i));
        } else if (order == REVERSE && new_value < _begin->getCoord(i)) {
            _cursor->setCoord(i, _end->getCoord(i));
        } else {
            _cursor->setCoord(i, new_value);
            break;
        }
    }

    if (i >= dim) {
        delete _cursor;
        _cursor = order == STRAIGHT ? _end->clone() : _begin->clone();
        return ReturnCode::RC_OUT_OF_BOUNDS;
    }

    return ReturnCode::RC_SUCCESS;
}

ReturnCode CompactImpl::contains(IVector const* vec, bool& result) const {
    if (vec->getDim() != _dim) {
        logError(_logger, ReturnCode::RC_WRONG_DIM);
        return ReturnCode::RC_WRONG_DIM;
    }

    for (int i = 0; i < _dim; ++i) {
        if (vec->getCoord(i) <= _begin->getCoord(i) || vec->getCoord(i) >= _end->getCoord(i)) {
            result = false;
            return ReturnCode::RC_SUCCESS;
        }
    }
    result = true;
    return ReturnCode::RC_SUCCESS;
}

ReturnCode CompactImpl::intersects(const ICompact *comp, bool &result) const {
    if (!comp) {
        logError(_logger, ReturnCode::RC_NULL_PTR);
        return ReturnCode::RC_NULL_PTR;
    }
    if (comp->getDim() != _dim) {
        logError(_logger, ReturnCode::RC_WRONG_DIM);
        return ReturnCode::RC_WRONG_DIM;
    }
    auto * comp_begin = comp->getBegin();
    auto * comp_end = comp->getEnd();
    ReturnCode code;

    result = true;
    for (size_t i = 0; i < _dim; ++i) {
        //check intersects of projections
        double begin_max = std::max(comp_begin->getCoord(i), _begin->getCoord(i));
        double end_min = std::min(comp_end->getCoord(i), _end->getCoord(i));
        if (begin_max > end_min) {
            result = false;
        }
    }
    return ReturnCode::RC_SUCCESS;
}

ReturnCode CompactImpl::isSubset(const ICompact *comp, bool &result) const {
    if (!comp) {
        logError(_logger, ReturnCode::RC_NULL_PTR);
        return ReturnCode::RC_NULL_PTR;
    }
    if (comp->getDim() != _dim) {
        logError(_logger, ReturnCode::RC_WRONG_DIM);
        return ReturnCode::RC_WRONG_DIM;
    }
    bool contains_begin, contains_end;
    ReturnCode code;
    if  ((code = comp->contains(_begin, contains_begin)) != ReturnCode::RC_SUCCESS) {
        logError(_logger, code);
        return code;
    }
    if ((code = comp->contains(_end, contains_end)) != ReturnCode::RC_SUCCESS) {
        logError(_logger, code);
        return code;
    }
    if (contains_begin && contains_end) {
        result = true;
    } else {
        result = false;
    }
    return ReturnCode::RC_SUCCESS;
}

CompactImpl::CompactImpl(const IVector *begin, const IVector *end) : _begin(begin), _end(end) {
    _dim = begin->getDim();
    _logger = ILogger::createLogger(this);
}

ICompact *CompactImpl::clone() const {
    return createCompact(_begin, _end, 0);
}

