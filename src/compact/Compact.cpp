//
// Created by markp on 16.09.2020.
//

#include <cmath>
#include "include/ICompact.h"
#include "CompactImpl.cpp"

ICompact* ICompact::createCompact(IVector const* begin, IVector const* end, double tolerance, ILogger* logger) {
    if (!begin || !end) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    if (begin->getDim() != end->getDim()) {
        logError(logger, ReturnCode::RC_WRONG_DIM);
        return nullptr;
    }
    size_t dim = begin->getDim();
    for (int i = 0; i < dim; ++i) {
        if (end->getCoord(i) - begin->getCoord(i) < tolerance) {
            logError(logger, ReturnCode::RC_INVALID_PARAMS);
            return nullptr;
        }
    }
    if (std::isnan(tolerance) || tolerance < 0.0) {
        logError(logger, ReturnCode::RC_INVALID_PARAMS);
        return nullptr;
    }
    bool is_equals = false;
    IVector::equals(begin, end, IVector::Norm::NORM_1, tolerance, is_equals);
    if (is_equals) {
        logError(logger, ReturnCode::RC_INVALID_PARAMS);
        return nullptr;
    }
    IVector * begin_copy = begin->clone();
    if (!begin_copy) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    IVector * end_copy = end->clone();
    if (!end_copy) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        delete begin_copy;
        return nullptr;
    }
    CompactImpl * result = new(std::nothrow) CompactImpl(begin_copy, end_copy);
    if (!result) {
        delete begin_copy;
        delete end_copy;
        logError(logger, ReturnCode::RC_NULL_PTR);
    }
    return (ICompact *)result;
}

static ReturnCode checkOperands(ICompact const* comp1, ICompact const* comp2) {
    if (!comp1 || !comp2) {
        return ReturnCode::RC_NULL_PTR;
    }
    if (comp2->getDim() != comp1->getDim() || comp1->getDim() == 0) {
        return ReturnCode::RC_WRONG_DIM;
    }
    return ReturnCode::RC_SUCCESS;
}

static bool onSameStraight(IVector * v1, IVector * v2, double tolerance, size_t & axes_number) {
    size_t dim = v1->getDim();
    size_t number_of_equals_coord = 0;
    for (int i = 0; i < dim; ++i) {
        if (std::fabs(v1->getCoord(i) - v2->getCoord(i)) < tolerance) {
            number_of_equals_coord++;
        } else {
            axes_number = i;
        }
    }
    if (number_of_equals_coord == dim - 1) {
        return true;
    } else {
        return false;
    }
}

ICompact* ICompact::_union(ICompact const* comp1, ICompact const* comp2, double tolerance, ILogger* logger) {
    ReturnCode code = checkOperands(comp1, comp2);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }

    auto begin1 = comp1->getBegin();
    if (!begin1) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    auto begin2 = comp2->getBegin();
    if (!begin2) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        delete begin1;
        return nullptr;
    }
    size_t begin_axes;
    if (!onSameStraight(begin1, begin2, tolerance, begin_axes)) {
        delete begin1;
        delete begin2;
        return nullptr;
    }
    auto end1 = comp1->getEnd();
    if (!end1) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    auto end2 = comp2->getEnd();
    if (!end2) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        delete begin1;
        return nullptr;
    }
    size_t end_axes;
    if (!onSameStraight(end1, end2, tolerance, end_axes)) {
        logError(logger, ReturnCode::RC_INVALID_PARAMS);
        delete begin1;
        delete begin2;
        return nullptr;
    }
    if (begin_axes != end_axes) {
        logError(logger, ReturnCode::RC_INVALID_PARAMS);
        delete begin1;
        delete begin2;
        delete end1;
        delete end2;
        return nullptr;
    }
    bool is_intersect;
    comp1->intersects(comp2, is_intersect);
    if (!is_intersect) {
        logError(logger, ReturnCode::RC_INVALID_PARAMS);
        delete end2;
        delete begin1;
        delete begin2;
        delete end1;
        return nullptr;
    }

    begin1->setCoord(begin_axes, std::min(begin1->getCoord(begin_axes), begin2->getCoord(begin_axes)));
    end1->setCoord(end_axes, std::max(end1->getCoord(end_axes), end2->getCoord(end_axes)));

    ICompact * result = ICompact::createCompact(begin1, end1, tolerance);
    if (!result) {
        logError(logger, ReturnCode::RC_NULL_PTR);
    }
    delete end2;
    delete begin1;
    delete begin2;
    delete end1;
    return result;
}

ICompact* ICompact::convex(ICompact const* comp1, ICompact const* comp2, double tolerance, ILogger* logger) {
    ReturnCode code = checkOperands(comp1, comp2);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }
    IVector * begin = comp1->getBegin();
    IVector * begin2 = comp2->getBegin();
    IVector * end = comp1->getEnd();
    IVector * end2 = comp2->getEnd();
    if (!begin || !end || !begin2 || !end2) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        delete begin;
        delete end;
        delete end2;
        delete begin2;
        return nullptr;
    }
    size_t dim = begin->getDim();
    for (int i = 0; i < dim; ++i) {
        begin->setCoord(i, std::min(begin->getCoord(i), begin2->getCoord(i)));
        end->setCoord(i, std::max(end->getCoord(i), end2->getCoord(i)));
    }
    ICompact * result = ICompact::createCompact(begin, end, tolerance);
    if (!result) {
        logError(logger, ReturnCode::RC_NULL_PTR);
    }
    delete begin;
    delete begin2;
    delete end;
    delete end2;
    return result;
}

ICompact* ICompact::intersection(ICompact const* comp1, ICompact const* comp2, double tolerance, ILogger* logger) {
    ReturnCode code = checkOperands(comp1, comp2);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }
    IVector * begin1 = comp1->getBegin();
    IVector * begin2 = comp2->getBegin();
    IVector * end1 = comp1->getEnd();
    IVector * end2 = comp2->getEnd();

    bool is_intersects;
    code = comp1->intersects(comp2, is_intersects);
    if (!is_intersects || code != ReturnCode::RC_SUCCESS) {
        logError(logger, ReturnCode::RC_INVALID_PARAMS);
        return nullptr;
    }
    size_t dim = comp1->getDim();
    for (int i = 0; i < dim; ++i) {
        begin1->setCoord(i, std::max(begin1->getCoord(i), begin2->getCoord(i)));
        end1->setCoord(i, std::min(end1->getCoord(i), end2->getCoord(i)));
    }
    ICompact * result = ICompact::createCompact(begin1, end1, tolerance);
    if (!result) {
        logError(logger, ReturnCode::RC_NULL_PTR);
    }
    return result;
}
ICompact::~ICompact() {}
ICompact::Iterator::~Iterator() {}
