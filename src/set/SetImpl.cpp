//
// Created by markp on 13.09.2020.
//
#include "include/ILogger.h"
#include "include/ISet.h"
#include <vector>
#include <cmath>

static void tryLog(ILogger * logger, const char * message, ReturnCode code) {
    if (!logger) {
        return;
    }
    logger->log(message, code);
}

static void logError(ILogger * logger, ReturnCode code) {
    switch (code) {
        case ReturnCode::RC_WRONG_DIM:
            tryLog(logger, "set: operands have different _dim", ReturnCode::RC_WRONG_DIM);
            break;
        case ReturnCode::RC_NO_MEM:
            tryLog(logger, "set: cannot allocate memory", ReturnCode::RC_WRONG_DIM);
            break;
        case ReturnCode::RC_NULL_PTR:
            tryLog(logger, "set: cannot operate nullptr", ReturnCode::RC_NULL_PTR);
            break;
        case ReturnCode::RC_INVALID_PARAMS:
            tryLog(logger, "set: invalid params", ReturnCode::RC_NULL_PTR);
            break;
    }
}

namespace {
    class SetImpl : ISet {
        size_t _dim;
        std::vector<const IVector *> _data;
        ILogger * _logger;
    public:
        SetImpl();

    private:
        ReturnCode insert(const IVector *vector, IVector::Norm norm, double tolerance) override;

        ReturnCode erase(const IVector *vector, IVector::Norm norm, double tolerance) override;

        ReturnCode erase(size_t ind) override;

        void clear() override;

        ReturnCode find(const IVector *vector, IVector::Norm norm, double tolerance, size_t &ind) const override;

        ReturnCode get(IVector *&dst, size_t ind) const override;

        size_t getDim() const override;

        size_t getSize() const override;

        ISet *clone() const override;

        ~SetImpl() override;
    };
}

SetImpl::SetImpl() {
    this->_logger = ILogger::createLogger(this);
    _dim = 0;
}

SetImpl::~SetImpl() {
    size_t len = _data.size();
    for (int i = 0; i < len; ++i) {
        delete _data[i];
    }
    _data.clear();
    _dim = 0;
    _logger->releaseLogger(this);
}

ISet * SetImpl::clone() const {
    ISet * copy = createSet(_logger);
    if (!copy) {
        //TODO log
        return nullptr;
    }
    size_t size = _data.size();
    for (int i = 0; i < size; ++i) {
        IVector * el = nullptr;
        get(el, i);
        copy->insert(el, IVector::Norm::NORM_1, 0);
    }
    return copy;
}

void SetImpl::clear() {
    for (auto it = _data.begin(); it != _data.end(); ++it) {
        delete *it;
    }
    _data.clear();
    _dim = 0;
}

size_t SetImpl::getSize() const {
    return _data.size();
}

size_t SetImpl::getDim() const {
    return _dim;
}

ReturnCode SetImpl::insert(const IVector *vector, IVector::Norm norm, double tolerance) {
    if (!vector || (vector->getDim() != _dim && !_data.empty()) || std::isnan(tolerance) || tolerance < 0) {
        logError(_logger, ReturnCode::RC_INVALID_PARAMS);
        return ReturnCode::RC_INVALID_PARAMS;
    }

    if (_data.empty()) {
        _data.push_back(vector->clone());
        _dim = vector->getDim();
        return ReturnCode::RC_SUCCESS;
    }

    size_t len = _data.size();
    for (int i = 0; i < len; ++i) {
        auto it = _data[i];
        bool isEqual = false;
        IVector::equals(it, vector, norm, tolerance, isEqual, _logger);

        if (isEqual) {
            return ReturnCode::RC_SUCCESS;
        }
    }

    _data.push_back(vector->clone());
    return ReturnCode::RC_SUCCESS;
}

ReturnCode SetImpl::erase(size_t ind) {
    if (ind < 0 || ind >= _data.size()) {
        return ReturnCode::RC_INVALID_PARAMS;
    }

    delete _data[ind];
    _data.erase(_data.begin() + ind);

    if (_data.empty()) {
        _dim = 0;
    }
    return ReturnCode::RC_SUCCESS;
}

ReturnCode SetImpl::erase(const IVector *vector, IVector::Norm norm, double tolerance) {
    if (!vector || (vector->getDim() != _dim && !_data.empty()) || std::isnan(tolerance)) {
        return ReturnCode::RC_INVALID_PARAMS;
    }

    size_t len = _data.size();
    for (int i = 0; i < len; ++i) {
        auto it = _data[i];
        bool isEqual = false;
        IVector::equals(it, vector, norm, tolerance, isEqual, _logger);

        if (isEqual) {
            return erase(i);
        }
    }
    return ReturnCode::RC_ELEM_NOT_FOUND;
}

ReturnCode SetImpl::find(const IVector *vector, IVector::Norm norm, double tolerance, size_t &ind) const {
    if (!vector || (vector->getDim() != _dim && !_data.empty()) || std::isnan(tolerance)) {
        return ReturnCode::RC_INVALID_PARAMS;
    }

    size_t len = _data.size();
    for (int i = 0; i < len; ++i) {
        auto it = _data[i];
        bool isEqual = false;
        IVector::equals(it, vector, norm, tolerance, isEqual, _logger);

        if (isEqual) {
            ind = i;
            return ReturnCode::RC_SUCCESS;
        }
    }
    return ReturnCode::RC_ELEM_NOT_FOUND;
}

ReturnCode SetImpl::get(IVector *&dst, size_t ind) const {
    if (ind >= _data.size()) {
        return ReturnCode::RC_OUT_OF_BOUNDS;
    }
    dst = _data[ind]->clone();
    return ReturnCode::RC_SUCCESS;
}