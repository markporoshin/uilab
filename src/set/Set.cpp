//
// Created by markp on 13.09.2020.
//

#include "include/ISet.h"
#include "SetImpl.cpp"

static ReturnCode checkOperands(const ISet *set1, const ISet *set2) {
    if (!set1 || !set2 || set1->getDim() != set2->getDim() || set1->getDim() == 0) {
        return ReturnCode::RC_INVALID_PARAMS;
    }
    return ReturnCode::RC_SUCCESS;
}

ISet * ISet::createSet(ILogger *logger) {
    ISet * result = (ISet *)new(std::nothrow) SetImpl();
    if (!result) {

        return nullptr;
    }
    return result;
}

ISet * ISet::_union(const ISet *set1, const ISet *set2, IVector::Norm norm, double tolerance, ILogger *logger) {
    auto code = checkOperands(set1, set2);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }

    ISet * result = set1->clone();
    size_t len2 = set2->getSize();
    for (int i = 0; i < len2; ++i) {
        IVector * v1;
        set2->get(v1, i);
        code = result->insert(v1, norm, tolerance);
        if (code != ReturnCode::RC_SUCCESS) {
            logError(logger, code);
            delete result;
            return nullptr;
        }
    }
    return result;
}

ISet * ISet::difference(const ISet *minuend, const ISet *subtrahend, IVector::Norm norm, double tolerance, ILogger *logger) {
    auto code = checkOperands(minuend, subtrahend);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }

    ISet * result = minuend->clone();
    if (!result) {
        logError(logger, ReturnCode::RC_NO_MEM);
        return nullptr;
    }
    size_t len = subtrahend->getSize();
    for (size_t i = 0; i < len; ++i) {
        IVector * v = nullptr;
        subtrahend->get(v, i);
        code = result->erase(v, norm, tolerance);
        if (code != ReturnCode::RC_SUCCESS && code != ReturnCode::RC_ELEM_NOT_FOUND) {
            logError(logger, ReturnCode::RC_ELEM_NOT_FOUND);
            delete result;
            return nullptr;
        }
    }

    return result;
}

ISet * ISet::symmetricDifference(const ISet *set1, const ISet *set2, IVector::Norm norm, double tolerance, ILogger *logger) {
    auto code = checkOperands(set1, set2);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }

    ISet * _u = _union(set1, set2, norm, tolerance, logger);
    ISet * _i = intersection(set1, set2, norm, tolerance, logger);

    ISet* result = difference(_u, _i, norm, tolerance, logger);

    if (!result) {
        logError(logger, ReturnCode::RC_NULL_PTR);
    }

    delete _u;
    delete _i;

    return result;
}

ISet * ISet::intersection(const ISet *set1, const ISet *set2, IVector::Norm norm, double tolerance, ILogger *logger) {
    if (!set1 || !set2 || set1->getDim() != set2->getDim() || set1->getDim() == 0) {

        return nullptr;
    }
    ISet * result = createSet(logger);
    if (!result) {
        return nullptr;
    }
    size_t len1 = set1->getSize(), len2 = set2->getSize();
    for (int i = 0; i < len1; ++i) {
        for (int j = 0; j < len2; ++j) {

            IVector * v1, * v2;
            auto result1 = set1->get(v1, i);
            auto result2 = set2->get(v2, j);
            if (result1 != ReturnCode::RC_SUCCESS || result2 != ReturnCode::RC_SUCCESS) {
                delete result;
                return nullptr;
            }
            bool isEqual = false;
            IVector::equals(v1, v2, norm, tolerance, isEqual, logger);
            if (isEqual) {
                auto code = result->insert(v1, norm, tolerance);
                if (code != ReturnCode::RC_SUCCESS) {
                    delete result;
                    return nullptr;
                }
            }
        }
    }

    return result;
}

ISet::~ISet() {}


