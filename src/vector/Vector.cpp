//
// Created by markp on 13.09.2020.
//

#include <new>
#include <cmath>
#include <cstring>
#include "include/IVector.h"
#include "VectorImpl.cpp"



static ReturnCode checkOperands(const IVector *addend1, const IVector *addend2) {
    if (!addend1 || !addend2) {
        return ReturnCode::RC_NULL_PTR;
    }
    if (addend1->getDim() != addend2->getDim()) {
        return ReturnCode::RC_WRONG_DIM;
    }
    return ReturnCode::RC_SUCCESS;
}

static ReturnCode checkOperand(const IVector *addend1) {
    if (!addend1) {
        return ReturnCode::RC_NULL_PTR;
    }
    return ReturnCode::RC_SUCCESS;
}

IVector * IVector::createVector(size_t dim, double *data, ILogger *logger) {
    if (!dim) {
        logError(logger, ReturnCode::RC_WRONG_DIM);
        return nullptr;
    }
    if (!data) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    for (int i = 0; i < dim; ++i) {
        if (std::isinf(data[i]) || std::isnan(data[i])) {
            logError(logger, ReturnCode::RC_NAN);
            return nullptr;
        }
    }

    double * copy = new(std::nothrow) double [dim];
    if (!copy) {
        logError(logger, ReturnCode::RC_NO_MEM);
        return nullptr;
    }
    std::memcpy(copy, data, dim * sizeof(double));
    IVector * result = (IVector *)new(std::nothrow) VectorImpl(dim, copy);
    if (!result) {
        logError(logger, ReturnCode::RC_NO_MEM);
        delete [] copy;
        return nullptr;
    }
    return result;
}

IVector * IVector::add(const IVector *addend1, const IVector *addend2, ILogger *logger) {
    auto code = checkOperands(addend1, addend2);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }

    size_t dim = addend1->getDim();
    auto * data = new(std::nothrow) double[dim];
    if (!data) {
        logError(logger, ReturnCode::RC_NO_MEM);
        return nullptr;
    }
    for (int i = 0; i < dim; ++i) {
        data[i] = addend1->getCoord(i) + addend2->getCoord(i);
        if (std::isnan(data[i])) {
            logError(logger, ReturnCode::RC_NO_MEM);
            delete [] data;
            return nullptr;
        }
    }
    IVector * result = createVector(dim, data, logger);
    if (!result) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        delete [] data;
        return nullptr;
    }
    return result;
}

IVector * IVector::mul(const IVector *multiplier, double scale, ILogger *logger) {
    auto code = checkOperand(multiplier);
    if (std::isnan(scale) || std::isinf(scale)) {
        code = ReturnCode::RC_NAN;
    }
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }
    auto result = multiplier->clone();
    if (!result) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    for (int i = 0; i < result->getDim(); ++i) {
        result->setCoord(i, result->getCoord(i) * scale);
    }
    return result;
}

IVector * IVector::sub(const IVector *minuend, const IVector *subtrahend, ILogger *logger) {
    auto code = checkOperands(minuend, subtrahend);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return nullptr;
    }

    size_t dim = minuend->getDim();
    auto * data = new(std::nothrow) double[dim];
    if (!data) {
        logError(logger, ReturnCode::RC_NO_MEM);
        return nullptr;
    }
    for (int i = 0; i < dim; ++i) {
        data[i] = minuend->getCoord(i) - subtrahend->getCoord(i);
        if (std::isnan(data[i])) {
            logError(logger, ReturnCode::RC_NO_MEM);
            delete [] data;
            return nullptr;
        }
    }
    IVector * result = createVector(dim, data, logger);
    delete [] data;
    if (!result) {
        logError(logger, ReturnCode::RC_NULL_PTR);
        return nullptr;
    }
    return result;
}

double IVector::mul(const IVector *multiplier1, const IVector *multiplier2, ILogger *logger) {
    auto code = checkOperands(multiplier1, multiplier2);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        return std::nan("1");
    }

    double res = 0;
    for (size_t i = 0; i < multiplier1->getDim(); i++) {
        res += multiplier1->getCoord(i) * multiplier2->getCoord(i);
    }
    return res;
}

ReturnCode IVector::equals(const IVector *v1, const IVector *v2, Norm norm, double tolerance, bool &result, ILogger *logger) {
    auto code = checkOperands(v1, v2);
    if (code != ReturnCode::RC_SUCCESS) {
        logError(logger, code);
        result = false;
        return code;
    }

    auto diff = sub(v1, v2, logger);
    if (!diff) {
        result = false;
        logError(logger, ReturnCode::RC_NULL_PTR);
        return ReturnCode::RC_NULL_PTR;
    }

    auto norm_value = std::fabs(diff->norm(norm));
    if (norm_value <= tolerance) {
        result = true;
    } else {
        result = false;
    }
    delete diff;
    return ReturnCode::RC_SUCCESS;
}

IVector::~IVector() {}