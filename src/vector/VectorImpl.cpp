//
// Created by markp on 13.09.2020.
//

#include <cmath>
#include "include/IVector.h"

static void tryLog(ILogger * logger, const char * message, ReturnCode code) {
    if (!logger) {
        return;
    }
    logger->log(message, code);
}

static void logError(ILogger * logger, ReturnCode code) {
    switch (code) {
    case ReturnCode::RC_WRONG_DIM:
        tryLog(logger, "vector: operands have different _dim", ReturnCode::RC_WRONG_DIM);
        break;
    case ReturnCode::RC_NO_MEM:
        tryLog(logger, "vector: cannot allocate memory", ReturnCode::RC_WRONG_DIM);
        break;
    case ReturnCode::RC_NULL_PTR:
        tryLog(logger, "vector: cannot operate nullptr", ReturnCode::RC_NULL_PTR);
        break;
    case ReturnCode::RC_INVALID_PARAMS:
        tryLog(logger, "vector: invalid params", ReturnCode::RC_NULL_PTR);
        break;
    case ReturnCode::RC_NAN:
        tryLog(logger, "vector: nan value", ReturnCode::RC_NULL_PTR);
        break;
    }
}


namespace {
    class VectorImpl : IVector {
        ILogger * _logger;
        double * _data;
        size_t _dim;
    public:
        VectorImpl(size_t dim, double * data);
        double getCoord(size_t index) const override;
        ReturnCode setCoord(size_t index, double value) const override;
        double norm(Norm norm) const override;
        size_t getDim() const override;
        IVector * clone() const override;
        ~VectorImpl() override;
    };
}

VectorImpl::VectorImpl(size_t dim, double * data) : _dim(dim), _data(data) {
    _logger = ILogger::createLogger(this);
}

VectorImpl::~VectorImpl() {
    delete _data;
    if (_logger)
        _logger->releaseLogger(this);
}

size_t VectorImpl::getDim() const {
    return _dim;
}

double VectorImpl::getCoord(size_t index) const {
    if (index >= _dim || index < 0) {
        logError(_logger, ReturnCode::RC_WRONG_DIM);
        return std::nan("1");
    }
    return _data[index];
}

ReturnCode VectorImpl::setCoord(size_t index, double value) const {
    if (index >= _dim || index < 0) {
        logError(_logger, ReturnCode::RC_INVALID_PARAMS);
        return ReturnCode::RC_INVALID_PARAMS;
    }
    if (std::isnan(value) || std::isinf(value)) {
        logError(_logger, ReturnCode::RC_NAN);
        return ReturnCode::RC_NAN;
    }
    _data[index] = value;
    return ReturnCode::RC_SUCCESS;
}

double VectorImpl::norm(Norm norm) const {
    double result = 0;
    switch (norm) {
        case Norm::NORM_1:
            for (size_t i = 0; i < _dim; ++i) {
                result += std::fabs(_data[i]);
            }
            break;
        case Norm::NORM_2:
            for (size_t i = 0; i < _dim; ++i) {
                result += _data[i] * _data[i];
            }
            result = std::sqrt(result);
            break;
        case Norm::NORM_INF:
            result = std::fabs(_data[0]);
            for (size_t i = 1; i < _dim; ++i) {
                if (result < std::fabs(_data[i]))
                    result = std::fabs(_data[i]);
            }
            break;
    }
    return result;
}

IVector * VectorImpl::clone() const {
    return createVector(_dim, _data, _logger);
}

