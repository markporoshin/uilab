//
// Created by markp on 13.09.2020.
//
#include <set>
#include <stdio.h>
#include "include/ILogger.h"

using namespace std;

namespace {
    class LoggerImpl : ILogger {

    protected:
        static LoggerImpl * _instance;
        FILE * _log_file;
        set<void *> _clients;
    public:
        static ILogger * getLogger(void * client);
        void log(const char *message, ReturnCode returnCode) override;
        void releaseLogger(void *client) override;
        ReturnCode setLogFile(const char *logFileName) override;
        ~LoggerImpl() override;
    };
    LoggerImpl * LoggerImpl::_instance = nullptr;
}

ILogger * LoggerImpl::getLogger(void *client) {
    if (!client) {
        //TODO logging
        return nullptr;
    }

    if (!LoggerImpl::_instance) {
        _instance = new(std::nothrow) LoggerImpl();
        if (!_instance) {
            //TODO logging
            return nullptr;
        }
        _instance->_log_file = stdout;
    }
    _instance->_clients.insert(client);
    return _instance;
}

void LoggerImpl::log(const char *message, ReturnCode returnCode) {
    if (!_instance) {
        return;
    }
    if (!message) {
        fprintf(_log_file, "fun=%s code=%d message=NULL ", __FUNCTION__, returnCode);
    }
    fprintf(_log_file, "fun=%s code=%d message=%s ", __FUNCTION__, returnCode ,message);
}

void LoggerImpl::releaseLogger(void *client) {
    if (!_instance) {
        return;
    }
    if (!client) {
        //TODO logging
        return;
    }
    auto client_iter = _clients.find(client);
    if (client_iter != _clients.end())
        _clients.erase(client_iter);
    if (_clients.empty()) {
        delete _instance;
        _instance = nullptr;
    }
}

LoggerImpl::~LoggerImpl() {
    if (!_instance) {
        return;
    }
    fflush(_log_file);
    if (_log_file != stdout)
        fclose(_log_file);
}

ReturnCode LoggerImpl::setLogFile(const char *logFileName) {
    if (!_instance) {
        return ReturnCode::RC_NULL_PTR;
    }
    if (_instance->_log_file != stdout) {
        if (!freopen(logFileName, "a", _instance->_log_file)) {
            return ReturnCode::RC_OPEN_FILE;
        }
    } else {
        _instance->_log_file = fopen(logFileName, "a");
        if (_instance->_log_file == NULL) {
            _instance->_log_file = stdout;
        }
    }
    return ReturnCode::RC_SUCCESS;
}