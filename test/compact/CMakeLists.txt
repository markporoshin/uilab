# сообщаем об .exe-шнике и из каких файлов он будет собран
add_executable(compact_test compact_test.cpp compact_test.h)

# подключаем все библиотеки которые тестируем
target_link_libraries(compact_test PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../../bin/lib/liblogger.dll.a)
target_link_libraries(compact_test PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../../bin/lib/libvector.dll.a)
target_link_libraries(compact_test PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../../bin/lib/libset.dll.a)
target_link_libraries(compact_test PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../../bin/lib/libcompact.dll.a)
