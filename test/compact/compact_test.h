//
// Created by markp on 21.09.2020.
//

#ifndef POROSHINUI_COMPACT_TEST_H
#define POROSHINUI_COMPACT_TEST_H

#include <iostream>
#include <vector>
#include <cmath>
#include "../include/ICompact.h"
#include "../test.h"

#define INIT_VECTORS_C \
const double tolerance = 1e-3; \
const size_t dim2 = 2, dim3 = 3; \
double data2_1[dim2] = {0, 0}; \
double data2_2[dim2] = {1, 1}; \
double data2_3[dim2] = {0, tolerance / 2}; \
double data2_4[dim2] = {0, 1}; \
double data2_5[dim2] = {1, 2}; \
double data2_6[dim2] = {2, 3}; \
double data2_7[dim2] = {1, 0}; \
double data2_8[dim2] = {3, 2}; \
double data2_9[dim2] = {2, 2}; \
double data2_10[dim2] = {4, 4}; \
double data3_1[dim3] = {1, 1, 1}; \
IVector * v2_1 = IVector::createVector(dim2, data2_1); \
IVector * v2_2 = IVector::createVector(dim2, data2_2); \
IVector * v3_1 = IVector::createVector(dim3, data3_1); \
IVector * v2_3 = IVector::createVector(dim2, data2_3); \
IVector * v2_4 = IVector::createVector(dim2, data2_4); \
IVector * v2_5 = IVector::createVector(dim2, data2_5); \
IVector * v2_6 = IVector::createVector(dim2, data2_6); \
IVector * v2_7 = IVector::createVector(dim2, data2_7); \
IVector * v2_8 = IVector::createVector(dim2, data2_8); \
IVector * v2_9 = IVector::createVector(dim2, data2_9); \
IVector * v2_10 = IVector::createVector(dim2, data2_10); \


#define DELETE_VECTORS_C \
delete v2_1; \
delete v2_2; \
delete v2_3; \
delete v2_4; \
delete v2_5; \
delete v3_1;

mp_test(compact_create) {
    INIT_VECTORS_C

    ICompact * compact1 = ICompact::createCompact(v2_1, v2_2, tolerance);
    mp_assert("just create", compact1 != nullptr)

    ICompact * compact2 = ICompact::createCompact(v2_2, v2_1, tolerance);
    mp_assert("create: begin > end", compact2 == nullptr)

    ICompact * compact3 = ICompact::createCompact(v3_1, v2_1, tolerance);
    mp_assert("create: diff dim of begin & end", compact3 == nullptr)

    ICompact * compact4 = ICompact::createCompact(v2_1, v2_3, tolerance);
    mp_assert("create: same 1st coord of begin & end", compact4 == nullptr)


    delete compact1;
    DELETE_VECTORS_C
}

mp_test(compact_union) {
    INIT_VECTORS_C
    ICompact * compact1 = ICompact::createCompact(v2_1, v2_2, tolerance);
    ICompact * compact2 = ICompact::createCompact(v2_4, v2_5, tolerance);
    ICompact * compact3 = ICompact::createCompact(v2_5, v2_6, tolerance);

    //     ┌─┐    ┌─┐
    //     ├─┤ -> │ │
    //     └─┘    └─┘
    ICompact * _u = ICompact::_union(compact1, compact2, tolerance);
    IVector * begin = _u->getBegin();
    IVector * end = _u->getEnd();
    mp_assert("valid union", _u != nullptr
                             && begin->getCoord(0) == 0 && begin->getCoord(1) == 0
                             && end->getCoord(0) == 1 && end->getCoord(1) == 2)
    delete begin;
    delete end;
    delete _u;

    _u = ICompact::_union(compact1, compact3, tolerance);
    mp_assert("invalid union", _u == nullptr);

    DELETE_VECTORS_C
}

mp_test(compact_convex) {
    INIT_VECTORS_C



    ICompact * compact1 = ICompact::createCompact(v2_4, v2_5, tolerance);
    ICompact * compact2 = ICompact::createCompact(v2_5, v2_6, tolerance);
    //        ┌─┐
    //     ┌─┐└─┘
    //     └─┘
    ICompact * convex = ICompact::convex(compact1, compact2, tolerance);
    mp_assert("convex: ", convex != nullptr);
    delete convex;
    delete compact1;
    delete compact2;

    ICompact * compact3 = ICompact::createCompact(v2_1, v2_2, tolerance);
    ICompact * compact4 = ICompact::createCompact(v2_7, v2_8, tolerance);
    //        ┌───┐(3, 2)
    //     ┌─┐│   │
    //(0,0)└─┘└───┘
    convex = ICompact::convex(compact3, compact4, tolerance);
    auto begin = convex->getBegin();
    auto end = convex->getEnd();
    mp_assert("convex: ", convex != nullptr &&
                          begin->getCoord(0) == 0 && begin->getCoord(1) == 0 &&
                          end->getCoord(0) == 3 && end->getCoord(1) == 2
    );
    delete begin;
    delete end;
    delete convex;

    DELETE_VECTORS_C
}

mp_test(compact_intersect) {
    INIT_VECTORS_C

    ICompact * compact1 = ICompact::createCompact(v2_1, v2_9, tolerance);
    ICompact * compact2 = ICompact::createCompact(v2_2, v2_10, tolerance);
    ICompact * i = ICompact::intersection(compact1, compact2, tolerance);
    auto begin = i->getBegin();
    auto end = i->getEnd();
    mp_assert("just intersection", i != nullptr &&
                                   begin->getCoord(0) == 1 && begin->getCoord(1) == 1 &&
                                   end->getCoord(0) == 2 && end->getCoord(1) == 2
    )
    delete begin;
    delete end;
    delete i;

    DELETE_VECTORS_C
}

mp_test(compact_contains) {
    INIT_VECTORS_C

    ICompact * compact1 = ICompact::createCompact(v2_1, v2_9, tolerance);
    bool is_contains = false;
    ReturnCode code = compact1->contains(v2_2, is_contains);
    mp_assert("1.", is_contains && code == ReturnCode::RC_SUCCESS)
    code = compact1->contains(v2_10, is_contains);
    mp_assert("2.", !is_contains && code == ReturnCode::RC_SUCCESS)

    DELETE_VECTORS_C
}



mp_test(compact_bypass) {
    INIT_VECTORS_C

    double data[] = {1e-1, 1e-1};
    double data2[] = {0.5, 0.5};
    IVector * step = IVector::createVector(2, data);
    IVector * mid = IVector::createVector(2, data2);

    ICompact * compact = ICompact::createCompact(v2_1, v2_2, tolerance);
    ICompact::Iterator * iterator = compact->begin(step);
    mp_assert("valid iterator", iterator);

    bool is_meet = false;
    while (iterator->doStep() == ReturnCode::RC_SUCCESS) {
        IVector * current = iterator->getPoint();

//        printf("{%lf %lf}\n", current->getCoord(0), current->getCoord(1));
//        fflush(stdout);

        bool is_equal = false;
        IVector::equals(current, mid, IVector::Norm::NORM_2, 0.1, is_equal);
        delete current;

        if (is_equal) {
            is_meet = true;
            break;
        }
    }
    mp_assert("meet {0.5 0.5} vector", is_meet);


    DELETE_VECTORS_C
}

#endif //POROSHINUI_COMPACT_TEST_H
