//
// Created by markp on 14.09.2020.
//

#ifndef POROSHINUI_TEST_H
#define POROSHINUI_TEST_H

#include <iostream>
#include <stdlib.h>
#include <vector>

static std::vector<void (*)()> tests{};
static int add_test(void (*fun)()) {
    tests.push_back(fun);
    return 0;
}

static void run_test() {
    for (auto test: tests) {
        test();
    }
}



#define mp_test(name) \
void test_##name();   \
void test_caller_##name();       \
void test_caller_##name() {      \
    printf("TEST_CASE %s:\n", (#name));                  \
    test_##name();    \
    fflush(stdout);                      \
}                     \
void (*(test_var_##name))() = test_caller_##name;                  \
static int __##name = add_test(test_var_##name);                      \
void test_##name()    \

#define mp_assert(name, e) \
printf("\tASSERT %s:\n\t\texpression: %s\n\t\tresult: %s\n", (#name), (#e), ((e) ? "SUCCESS" : "FAIL"));
#endif //POROSHINUI_TEST_H
