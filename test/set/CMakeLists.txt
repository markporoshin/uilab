# сообщаем об .exe-шнике и из каких файлов он будет собран
add_executable(set_test set_test.cpp set_test.h)

# подключаем все библиотеки которые тестируем
target_link_libraries(set_test PUBLIC ../../bin/lib/liblogger.dll.a)
target_link_libraries(set_test PUBLIC ../../bin/lib/libvector.dll.a)
target_link_libraries(set_test PUBLIC ../../bin/lib/libset.dll.a)

# .exe-шник должен перекинуться в bin
#set_target_properties(set_test PROPERTIES
#        RUNTIME_OUTPUT_DIRECTORY_DEBUG ..\\..\\bin
#        RUNTIME_OUTPUT_DIRECTORY_RELEASE ..\\..\\bin
#)

