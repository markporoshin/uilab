//
// Created by markp on 21.09.2020.
//

#ifndef POROSHINUI_SET_TEST_H
#define POROSHINUI_SET_TEST_H

#include <iostream>
#include <vector>
#include <cmath>
#include "../include/ILogger.h"
#include "../include/IVector.h"
#include "../include/ISet.h"
#include "../test.h"

#define INIT_VECTORS \
double tolerance = 1e-6; \
IVector::Norm norm = IVector::Norm::NORM_1; \
const int dim1 = 3; \
double data1[dim1] = {1, 2, 3}; \
double data1_1[dim1] = {1+1e-8, 2, 3}; \
double data2[dim1] = {7, -9, 4}; \
const int dim2 = 4; \
double data3[dim2] = {4, 5, 6, 7}; \
double data4[dim1] = {4, 5, 6}; \
IVector * v1 = IVector::createVector(dim1, data1); \
IVector * v1_1 = IVector::createVector(dim1, data1); \
IVector * v2 = IVector::createVector(dim1, data2); \
IVector * v3 = IVector::createVector(dim2, data3); \
IVector * v4 = IVector::createVector(dim1, data4);

#define DELETE_VECTORS \
delete v1; \
delete v1_1; \
delete v2; \
delete v3; \
delete v4;

mp_test(set_insert) {
    INIT_VECTORS
    ISet * set = ISet::createSet();

    auto result_code = set->insert(v1, norm, tolerance);
    mp_assert("first insert", result_code == ReturnCode::RC_SUCCESS && set->getDim() == v1->getDim());

    result_code = set->insert(v1_1, norm, tolerance);
    mp_assert("insert simplify vector", set->getSize() == 1)

    result_code = set->insert(v2, norm, tolerance);
    mp_assert("second insert(same dim)", result_code == ReturnCode::RC_SUCCESS);

    result_code = set->insert(v3, norm, tolerance);
    mp_assert("incorrect insert(bad dim)", result_code != ReturnCode::RC_SUCCESS);

    result_code = set->insert(nullptr, norm, tolerance);
    mp_assert("incorrect insert(nullptr)", result_code != ReturnCode::RC_SUCCESS);

    delete set;
    DELETE_VECTORS
}

mp_test(set_erase_by_index) {
    INIT_VECTORS
    ISet * set = ISet::createSet();

    set->insert(v1, norm, tolerance);
    auto return_code = set->erase(0);
    mp_assert("empty set after erase", return_code == ReturnCode::RC_SUCCESS)

    return_code = set->insert(v3, norm, tolerance);
    mp_assert("change dim after erase last vector", return_code == ReturnCode::RC_SUCCESS)

    delete set;
    DELETE_VECTORS
}

mp_test(set_erase_by_vector) {
    INIT_VECTORS
    ISet * set = ISet::createSet();

    set->insert(v2, norm, tolerance);
    set->insert(v1, norm, tolerance);
    set->erase(v1_1, norm, tolerance);
    mp_assert("erase simplify vector", set->getSize() == 1)

    delete set;
    DELETE_VECTORS
}

mp_test(set_find) {
    INIT_VECTORS
    ISet * set = ISet::createSet();

    set->insert(v2, norm, tolerance);
    set->insert(v1, norm, tolerance);

    size_t ind = -1;
    set->find(v1_1, norm, tolerance, ind);
    mp_assert("find simplify vector", ind != -1)

    ReturnCode result_code = set->find(v4, norm, tolerance, ind);
    mp_assert("find different vector", result_code != ReturnCode::RC_SUCCESS)

    delete set;
    DELETE_VECTORS
}

mp_test(set_clone) {
    INIT_VECTORS
    ISet * set = ISet::createSet();

    set->insert(v2, norm, tolerance);
    set->insert(v1, norm, tolerance);

    ISet * copy = set->clone();

    size_t ind = -1;
    ReturnCode return_code = copy->find(v1, norm, tolerance, ind);
    mp_assert("find v1", return_code == ReturnCode::RC_SUCCESS)
    return_code = copy->find(v2, norm, tolerance, ind);
    mp_assert("find v2", return_code == ReturnCode::RC_SUCCESS)

    delete set;
    delete copy;
    DELETE_VECTORS
}

mp_test(set_union) {
    INIT_VECTORS
    ISet * set1 = ISet::createSet();
    ISet * set2 = ISet::createSet();

    set1->insert(v2, norm, tolerance);
    set2->insert(v1, norm, tolerance);

    ISet * _union = ISet::_union(set1, set2, norm, tolerance);
    size_t ind = -1;
    ReturnCode return_code = _union->find(v1, norm, tolerance, ind);
    mp_assert("find v1 in union of {v1} v {v2}", return_code == ReturnCode::RC_SUCCESS)
    return_code = _union->find(v2, norm, tolerance, ind);
    mp_assert("find v2 in {v1} v {v2}", return_code == ReturnCode::RC_SUCCESS)

    set1->insert(v1, norm, tolerance);
    ISet * _union2 = ISet::_union(set1, set2, norm, tolerance);
    mp_assert("size of {v1, v2} v {v2} is 2", _union2->getSize() == 2)

    delete _union2;
    delete _union;
    delete set1;
    delete set2;
    DELETE_VECTORS
}

mp_test(set_difference) {
    INIT_VECTORS
    ISet * set1 = ISet::createSet();
    ISet * set2 = ISet::createSet();

    set1->insert(v1, norm, tolerance);
    set1->insert(v2, norm, tolerance);
    set2->insert(v1, norm, tolerance);

    ISet * diff = ISet::difference(set1, set2, norm, tolerance);
    size_t ind = -1;
    ReturnCode return_code = diff->find(v2, norm, tolerance, ind);
    mp_assert("find v2 in {v1, v2} \\ {v1}", return_code == ReturnCode::RC_SUCCESS)
    return_code = diff->find(v1, norm, tolerance, ind);
    mp_assert("not find v1 in {v1, v2} \\ {v2}", return_code != ReturnCode::RC_SUCCESS)
    mp_assert("size of {v1, v2} \\ {v2} is 1", diff->getSize() == 1)

    DELETE_VECTORS
}

mp_test(set_intersection) {
    INIT_VECTORS
    ISet * set1 = ISet::createSet();
    ISet * set2 = ISet::createSet();

    set1->insert(v1, norm, tolerance);
    set1->insert(v2, norm, tolerance);
    set2->insert(v1, norm, tolerance);

    ISet * intersection = ISet::intersection(set1, set2, norm, tolerance);
    size_t ind = -1;
    ReturnCode return_code = intersection->find(v1, norm, tolerance, ind);
    mp_assert("find v1 in {v1, v2} /\\ {v1}", return_code == ReturnCode::RC_SUCCESS)
    return_code = intersection->find(v2, norm, tolerance, ind);
    mp_assert("not find v2 in {v1, v2} /\\ {v2}", return_code != ReturnCode::RC_SUCCESS)
    mp_assert("size of {v1, v2} /\\ {v2} is 1", intersection->getSize() == 1)

    DELETE_VECTORS
}

mp_test(set_symmetricDifference) {
    INIT_VECTORS
    ISet * set1 = ISet::createSet();
    ISet * set2 = ISet::createSet();

    set1->insert(v1, norm, tolerance);
    set1->insert(v2, norm, tolerance);
    set2->insert(v1, norm, tolerance);
    set2->insert(v4, norm, tolerance);

    ISet * symm = ISet::symmetricDifference(set1, set2, norm, tolerance);
    size_t ind = -1;
    ReturnCode return_code = symm->find(v2, norm, tolerance, ind);
    mp_assert("find v2 in {v1, v2} sym {v1, v4}", return_code == ReturnCode::RC_SUCCESS)
    return_code = symm->find(v1, norm, tolerance, ind);
    mp_assert("not find v1 in {v1, v2} sym {v2, v4}", return_code != ReturnCode::RC_SUCCESS)
    mp_assert("size of {v1, v2} sym {v2, v4} is 1", symm->getSize() == 2)

    DELETE_VECTORS
}

#endif //POROSHINUI_SET_TEST_H
