//
// Created by markp on 21.09.2020.
//
#include "../include/ILogger.h"
#include "../include/IVector.h"
#include "../include/ISet.h"


int main() {
    ILogger * logger = ILogger::createLogger(nullptr);
    IVector * vector = IVector::createVector(0, nullptr);
    ISet * set = ISet::createSet();
    return 0;
}