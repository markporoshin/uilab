//
// Created by markp on 21.09.2020.
//

#include "../vector/vector_test.h"
#include "../set/set_test.h"
#include "../compact/compact_test.h"

int main() {
    run_test();
    return 0;
}