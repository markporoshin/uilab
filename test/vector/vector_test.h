//
// Created by markp on 21.09.2020.
//

#ifndef POROSHINUI_VECTOR_TEST_H
#define POROSHINUI_VECTOR_TEST_H

#include <iostream>
#include <vector>
#include <cmath>
#include "../include/ILogger.h"
#include "../include/IVector.h"
#include "../test.h"

mp_test(vector_check_dim) {
    double data[] = {1.0, 2.0, 3.0};
    IVector * v1 = IVector::createVector(0, data, nullptr);
    mp_assert(zero_dim, v1 == nullptr)

    IVector * v2 = IVector::createVector(3, data, nullptr);
    mp_assert(correct_dim, v2 != nullptr)
    delete v2;
}

mp_test(vector_check_data) {
    IVector * v1 = IVector::createVector(1, nullptr, nullptr);
    mp_assert("null data", v1 == nullptr)

    IVector * v2 = IVector::add(nullptr, nullptr);
    mp_assert("null + null == null", v2 == nullptr)

    IVector * v3 = IVector::sub(nullptr, nullptr);
    mp_assert("null - null == null", v3 == nullptr)

    IVector * v4 = IVector::mul(nullptr, 2);
    mp_assert("null * 2 == null", v4 == nullptr)

    double scalar_mul = IVector::mul(nullptr, nullptr);
    mp_assert("null * null == nan", std::isnan(scalar_mul))

    double data1[1] = {1.0};
    double data2[3] = {0.0, 3.0, 3.0};
    size_t dim1 = 1;
    size_t dim2 = 3;

    auto left = IVector::createVector(dim1, data1, nullptr);
    mp_assert("left operand", left != nullptr)

    auto right = IVector::createVector(dim2, data2, nullptr);
    mp_assert("right operand", right != nullptr)

    IVector * v5 = IVector::add(nullptr, nullptr);
    mp_assert("[1] + [0, 3, 3] == null", v5 == nullptr)

    IVector * v6 = IVector::sub(nullptr, nullptr);
    mp_assert("[1] - [0, 3, 3] == null", v6 == nullptr)

    double scalar_mul_2 = IVector::mul(nullptr, nullptr);
    mp_assert("[1] * [0, 3, 3] ==  nan", std::isnan(scalar_mul_2))

    delete left;
    delete right;
}

mp_test(vector_check_operations) {
    double data1[3] = {1.0, 2.0, 3.0};
    double data2[3] = {0.0, 3.0, 3.0};
    size_t dim = 3;

    auto v1 = IVector::createVector(dim, data1, nullptr);
    mp_assert("left operand", v1 != nullptr)
    auto v2 = IVector::createVector(dim, data2, nullptr);
    mp_assert("right operand", v2 != nullptr)

    auto summ = IVector::add(v1, v2, nullptr);
    mp_assert("v1 + v2 != nullptr", summ != nullptr)
    mp_assert("v1 + v2 correct value", summ->getDim() == 3 && summ->getCoord(0) == 1 && summ->getCoord(1) == 5 && summ->getCoord(2) == 6)

    auto diff = IVector::sub(v1, v2, nullptr);
    mp_assert("v1 - v2 != nullptr", diff != nullptr)
    mp_assert("v1 - v2 correct value", diff->getDim() == 3 && diff->getCoord(0) == 1 && diff->getCoord(1) == -1 && diff->getCoord(2) == 0)

    auto answer = IVector::mul(v1, v2, nullptr);
    mp_assert("v1 * v2 != nan", !std::isnan(answer))
    mp_assert("v1 * v2 correct value", answer == 15)

    auto mul = IVector::mul(v1, 2, nullptr);
    mp_assert("v1 * 2 != nullptr", mul != nullptr)
    mp_assert("v1 * 2 correct value", mul->getDim() == 3 && mul->getCoord(0) == 2 && mul->getCoord(1) == 4 && mul->getCoord(2) == 6)

    auto clone = v1->clone();
    mp_assert("v1->clone() != null", clone != nullptr)
    mp_assert("v1->clone() correct value", clone->getDim() == 3 && clone->getCoord(0) == v1->getCoord(0))
    bool isEquals = false;
    IVector::equals(clone, v1, IVector::Norm::NORM_1, 1e-6, isEquals);
    mp_assert("clone equals v1 (norm 1)", isEquals);
    IVector::equals(clone, v1, IVector::Norm::NORM_2, 1e-6, isEquals);
    mp_assert("clone equals v1 (norm 2)", isEquals);
    IVector::equals(clone, v1, IVector::Norm::NORM_INF, 1e-6, isEquals);
    mp_assert("clone equals v1 (norm inf)", isEquals);


    delete clone;
    delete v1;
    delete v2;
    delete diff;
    delete summ;
    delete mul;
}

#endif //POROSHINUI_VECTOR_TEST_H
